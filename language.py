# stdlib
import re


class Language(object):
	pass


class Latin(Language):
	RE_SPLITWORDS = re.compile("\s*[.,?!:;_/\"()-]+\s*|\s+'\s*|\s*'\s+|\s+")

	@staticmethod
	def is_question(sentence):
		return sentence.strip().endswith('?')

	@staticmethod
	def strip_question(sentence):
		return sentence.strip().lstrip('\u00bf').rstrip('?').strip()

	@classmethod
	def splitwords(klass, sentence):
		return tuple(filter(None, klass.RE_SPLITWORDS.split(sentence)))


class English(Latin):
	pass


if __name__ == '__main__':
	print(repr(Latin.splitwords("What's up, folks? Can we have 'some quotes' in this.example?")))
