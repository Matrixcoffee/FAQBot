# FAQBot
[FAQBot](https://gitlab.com/Matrixcoffee/FAQBot) is a [Matrix](https://matrix.org)
[bot](https://en.wikipedia.org/wiki/Chatbot) that answers questions, using one
or more [org-mode](http://orgmode.org) formatted files as its source of knowledge.

## Why does it exist?
FAQBot is here to enrich your rooms with knowledge

* of resources such as urls, which need looking up. Now, just ask FAQBot!
* at moments when there are no humans around to answer.
* just because.

## Status
**Late alpha**. (It has basic functionality and is stable, but still lacking in
many areas. It still has some quirks and hard-coded things that shouldn't be.)

## Recommended Installation
```
# Prerequisites
$ apt-get install git
$ apt-get install bash
$ apt-get install python3

# Main components
$ mkdir coffee_env
$ cd coffee_env
$ git clone https://gitlab.com/Matrixcoffee/FAQBot.git
$ git clone https://gitlab.com/Matrixcoffee/matrix-client-core.git
$ git clone https://gitlab.com/Matrixcoffee/transformat.git
$ git clone https://github.com/matrix-org/matrix-python-sdk.git -b v0.3.2

# ONE of the following:
$ git clone https://gitlab.com/Matrixcoffee/urllib-requests-adapter.git # (recommended)
$ apt-get install python3-requests

# If you want to serve the same questions as the "official" FAQBot:
$ git clone https://gitlab.com/Matrixcoffee/extract-web-to-org.git
$ git clone https://gitlab.com/Matrixcoffee/matrix-knowledge-base.git
```
If you want to run this on a [Tails](https://tails.boum.org) system, or otherwise need to connect through a SOCKS proxy, please refer to the installation instructions for [urllib-requests-adapter](https://gitlab.com/Matrixcoffee/urllib-requests-adapter).

Configuring:
```
$ cd FAQBot
$ /bin/sh faqbot.sh --write-config # WARNING: WILL OVERWRITE ANY EXISTING CONFIG
$ vi config.json # (kind of optional for a quick test run)
```
Running it:
```
$ /bin/sh faqbot.sh
```
(You need to configure FAQBot before it will run.)

FAQBot does not know how to register an account, so you must provide
one by other means. Use [Riot.im](https://riot.im/app), for example, to register an account and set a displayname, avatar, etc.

Enjoy!

## Contributing
Anyone can contribute to the project. Please send [MR](https://gitlab.com/help/user/project/merge_requests/index.md)s, or open an issue on the [tracker](https://gitlab.com/Matrixcoffee/FAQBot/issues). FAQBot has a [Matrix](https://matrix.org) room at [#faqbot:matrix.org](https://matrix.to/#/#faqbot:matrix.org).

## License
Copyright 2018 [@Coffee:matrix.org](https://matrix.to/#/@Coffee:matrix.org)

   > Licensed under the Apache License, Version 2.0 (the "License");
   > you may not use this file except in compliance with the License.

   > The full text of the License can be obtained from the file called [LICENSE](LICENSE).

   > Unless required by applicable law or agreed to in writing, software
   > distributed under the License is distributed on an "AS IS" BASIS,
   > WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   > See the License for the specific language governing permissions and
   > limitations under the License.
