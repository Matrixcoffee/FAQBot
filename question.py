# stdlib
import difflib
import re

# in-tree deps
import orgmode
from language import English as lang


class SearchIndex:
	def __init__(self, orgdoc):
		self.orgdoc = orgdoc
		self.items = {}

		obj = orgdoc.rootnode
		while True:
			if obj is None: break

			if len(obj.bodytext) > 0 \
			 and 'faqbot-skip' not in obj.properties \
			 and lang.is_question(obj.text):
				self.add(obj)

				# Question added. Skip any deeper levels.
				if obj.next_node is not None:
					obj = obj.next_node
				else:
					obj = obj.next()
				continue
			obj = obj.next()

		print("{} questions loaded.".format(len(self.items)))

	def add(self, item):
		self.items[item.text.lower()] = item

	@staticmethod
	def calc_score(s1, s2):
		if (s1 == s2): return 300
		# FIXME: we can do better by caching this
		words1 = s1.split()
		words2 = s2.split()
		if words1 == words2: return 200

		sm = difflib.SequenceMatcher()
		score = 0

		for w1 in words1:
			sm.set_seq1(w1)
			best = 0
			for w2 in words2:
				sm.set_seq2(w2)
				r = sm.ratio()
				if (r > best): best = r
			score += best

		for w1 in words2:
			sm.set_seq1(w1)
			best = 0
			for w2 in words1:
				sm.set_seq2(w2)
				r = sm.ratio()
				if (r > best): best = r
			score += best

		return 100 * score / (len(words1) + len(words2))

	def search(self, text, cutoff=50.0):
		results = []
		ltext = text.lower()
		words = lang.splitwords(ltext)
		for obj in self.items.values():
			if text == obj.text:
				perc = 500
				continue
			lwords = obj.text.lower()
			if ltext == lwords:
				perc = 400
				continue
			owords = lang.splitwords(lwords)
			perc = self.calc_score(ltext, lwords)
			if (perc >= cutoff):
				results.append((int(perc), obj))

		results.sort(reverse=True)
		if len(results) > 3: results = results[0:3]
		return results

	def fast_search(self, text):
		#100% match only
		return self.items.get(text.lower())


class Response:
	def __init__(self, reply=None):
		self.reply = reply
		self.followup_keys = {}
		self.followup_matchers = []

	def has_followup(self):
		return self.followup_keys or self.followup_matchers

	def match_followup(self, text):
		try: return self.followup_keys[text]
		except KeyError: pass

		for m in self.followup_matchers:
			reply = m(text)
			if reply is not None: return reply

		return None


class NumMatcher:
	NUMMATCHERS = {}
	for n in range(1, 10):
		NUMMATCHERS[n] = re.compile("^%s\\)?$" % n)

	def __init__(self, number, reply):
		self._search = self.NUMMATCHERS[number].search
		self.reply = reply

	def search(self, text):
		if self._search(text): return self.reply
		return None


class QuestionHandler:
	def __init__(self):
		self.filenames = set()
		self.orgdoc = orgmode.OrgDocument()
		self.idx = None

	def add_org_file(self, filename):
		self.orgdoc.readfromfile(filename)
		self.filenames.add(filename)

	def generate_index(self):
		self.idx = SearchIndex(self.orgdoc)

	@staticmethod
	def can_answer_directly(matches):
		if len(matches) < 1: raise IndexError("Must call with at least one result!")
		if len(matches) == 1: return matches[0][0] > 70
		if matches[0][0] >= 100: return matches[0][0] > matches[1][0]
		if matches[0][0] >= 95 and matches[1][0] < 70: return True
		if matches[0][0] >= 90 and matches[1][0] < 65: return True
		return False

	def ask(self, question, fast_only=False, user=None):
		response = None
		result = None
		if self.idx is None: self.generate_index()

		fast_match = self.idx.fast_search(question)
		if fast_match:
			result = []
			for line in fast_match.bodytext:
				result.append(line)
			return Response(result)

		if fast_only: return None

		matches = self.idx.search(question)
		if len(matches) > 0:
			long_reply = "{}: {} results".format(user, len(matches))
			for n, (score, obj) in enumerate(matches):
				long_reply = "\n".join((long_reply, "{}) [{:4}%] {}".format(n + 1, score, obj.text)))

			result = []
			if self.can_answer_directly(matches):
				if matches[0][0] <= 100:
					result.append("{}:\n> {}".format(user, matches[0][1].text))
				print(long_reply)
				for line in matches[0][1].bodytext:
					result.append(line)
				response = Response(result)
			else:
				result.append(long_reply)
				response = Response(result)
				for n, m in enumerate(matches):
					nm = NumMatcher(n + 1, m[1].bodytext).search
					response.followup_matchers.append(nm)

		return response


if __name__ == '__main__':
	print("Hello!")

	qh = QuestionHandler()
	qh.add_org_file("../../matrix-knowledge-base/MatrixKB.org")
	qh.add_org_file("../extract-web-to-org/untracked/trymatrixnow.org")
	qh.generate_index() # prevent delay before replying to first question

	import transformat
	import transformat.parser

	while True:
		text = input()
		if text.lower() in ("quit", "!quit", "/quit", "exit", "!exit", "/exit", ""):
			break

		r = qh.ask(text)
		if r is None:
			print("no reply")
			continue

		for line in r.reply:
			f = transformat.parser.OrgElementParser.parse(line)
			print(f.as_text())
			print(f.as_html())
