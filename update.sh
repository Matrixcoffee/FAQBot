#!/bin/bash

# Update all sources of knowledge

failed ()
{
	echo "Failed" "$@"
	exit 1
}

try ()
{
	echo "> $*"
	"$@" || failed to execute \""$@"\"
}

is_fresh()
{
	find "$@" -mmin -120 -print -quit | grep '' > /dev/null
}

STARTDIR="$( pwd )"

try cd ../matrix-knowledge-base
is_fresh || try git pull
try cd "$STARTDIR"

try cd ../extract-web-to-org
is_fresh || try /bin/bash run-extractors.bash
try cd "$STARTDIR"
