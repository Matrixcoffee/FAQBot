* FAQBot
** Are you open source?

Yes I am! My code is available from [[https://gitlab.com/Matrixcoffee/FAQBot]].

*** Is FAQBot open source?

Yes I am! My code is available from [[https://gitlab.com/Matrixcoffee/FAQBot]].

** How do you work?

I will answer your question unprompted if you get it exactly right.

You can also mention me and ask me a question directly, and I will try my best to find a suitable answer.

If I am not sure what you are asking, I will show you a list of possible questions. Just type the number in front of the question, and I will tell you the answer. You can also just re-ask the question.

** How old are you?

The first commit to my repository was on Wed Feb 21 17:22:24 2018 UTC.
I learned how to listen and reply to Matrix messages on Thu Feb 22 14:22:25 2018 UTC.

** Can you hear me?

I hear you.

** Are you awake?

I am listening and ready to answer questions.

** Are you alive?

I hope to be one day.

